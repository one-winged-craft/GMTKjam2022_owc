# SPDX-FileCopyrightText: 2022 Adriel Dumas--Jondeau <adrieldj@orange.fr>
# SPDX-FileCopyrightText: 2022 Luc Deligne <lucdeligne@hotmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

extends Node2D

export(int) var current_level = 0

var levels := [
		preload("res://Levels/Level0.tscn"),
		preload("res://Levels/Level1.tscn"),
		preload("res://Levels/Level2.tscn"),
		preload("res://Levels/Level3.tscn"),
		preload("res://Levels/Level4.tscn"),
		preload("res://Levels/Level5.tscn"),
		preload("res://Levels/Level6.tscn")
#		preload("res://Levels/TestLevel.tscn")
	]
var level: Node2D
var is_won := false
var controller := preload("res://Player/PlayerInput.tscn")

onready var control: Node2D = $CanvasLayer/PlayerInput
onready var text: Node2D = $CanvasLayer/EndText
onready var fade: Node2D = $CanvasLayer/ScreenFade


func _ready():
	setup_level(current_level)


func setup_level(index: int):
	control.cleanup()
	if not level == null: level.free()
	if index >= levels.size(): return
	level = levels[index].instance()
	control.queue_free()
	control = controller.instance()
	$CanvasLayer.add_child(control)
	control.level = level
	control.remote_front.frame = 6 - level.faces
	control.global_position = Vector2(15, 113)
	# warning-ignore:return_value_discarded
	level.connect("game_ended", self, "_on_game_ended")
	add_child(level)


func _on_game_ended(result):
	level.disconnect("game_ended", self, "_on_game_ended")
	match result:
		"win":
			control.state = 1
			control.is_boost = false
			text.won_text()
			is_won = true
		"lose":
			text.lost_text()
			is_won = false


func _on_next_level():
	if is_won: current_level += 1
	is_won = false
	setup_level(current_level)


func _on_EndText_animation_over():
	fade.fade()
