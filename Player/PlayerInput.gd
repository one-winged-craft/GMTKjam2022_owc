# SPDX-FileCopyrightText: 2022 Adriel Dumas--Jondeau <adrieldj@orange.fr>
# SPDX-FileCopyrightText: 2022 Luc Deligne <lucdeligne@hotmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

extends Node2D


export(int) var default_faces = 6
export(float) var blink_time = 0.1
export(float) var orient_delay = 0.3
export(float) var move_time = 0.1
export(float) var choose_time = 0.3
export(int) var random_rounds = 2
export(Resource) var sprite = preload("res://Player/Arrow.tscn")
export(int) var size = 8
export(int) var spacing_x = 4
export(int) var spacing_y = 2
export(float) var bck_scroll := 0.14

enum { STILL, WAITING_INPUT, BLINKING_ARROW,
 MOVING_FIRST_ARROW, WAIT_MOVING_ARROW, MOVING_PLAYER, BOOSTING_PLAYER
 DROP_ARROW, SHIFT_ARROWS, CHOOSING_MOVE, ORIENT_ARROWS,
 SPAWNING_HINT_ARROW, WAIT_HINT_ARROW
}

var state := WAITING_INPUT
var arrows := []
var rng := RandomNumberGenerator.new()
var player_direction := Vector2.ZERO
var is_boost := false
var random_round_current = null
var random_round_target = null

onready var tween := $Tween
onready var timer := $Timer
onready var big_origin := $ScreenBigOrigin
onready var small_origin := $ScreenSmallOrigin
onready var remote_front := $Remote/ScreenFront
onready var level := get_node_or_null("Level")


func _process(_delta):
	match state:
		WAITING_INPUT: _process_input()
		BLINKING_ARROW: _process_blinking_arrow()
		MOVING_FIRST_ARROW: _process_moving_first_arrow()
		WAIT_MOVING_ARROW: _process_wait_moving_arrow()
		MOVING_PLAYER: _process_moving_player()
		SPAWNING_HINT_ARROW: _process_spawning_hint_arrow()
		WAIT_HINT_ARROW: _process_wait_hint_arrow()
		SHIFT_ARROWS: _process_shift_arrows()
	_animate_backgrounds()


func cleanup():
	randomize()
	state = WAITING_INPUT
	random_round_current = null; random_round_target = null
	is_boost = false
	tween.stop_all()
	$Selector.visible = false
	$Hint.visible = false
	remove_arrows()


func commit(direction: Vector2, position: Vector2) -> void:
	var arrow = sprite.instance()
	arrows.push_front({ direction = direction, ref = arrow})
	add_child(arrow)
	arrow.global_position = position
	arrow.start_blinking()
	arrow.sprite_rotation_degrees = - rad2deg(direction.angle_to(Vector2.RIGHT))
	state = BLINKING_ARROW


func request() -> Vector2:
	return arrows[rng.randi_range(0, arrows.size() - 1)].direction



func remove_arrows():
	for arrow in arrows:
		arrow.ref.queue_free()
	arrows = []

func position_below(vec: Vector2) -> Vector2:
	return vec + Vector2.DOWN * (size + spacing_y)


func interpolate_global_position(
		obj: Node2D, targ: Vector2, t: float,
		trans_type := Tween.TRANS_LINEAR, ease_type := Tween.EASE_IN_OUT):
	# Helps creating interpolation on the global_position with the global
	# tween object. Please note that you still need to call `tween.start()`
	# manually in order to trigger the interpolation.
	tween.interpolate_property(obj, "global_position",
		obj.global_position, targ, t, trans_type, ease_type
	)


func _process_blinking_arrow():
	if arrows.size() == 0:
		state = WAITING_INPUT
		return
	if timer.time_left <= 0:
		timer.wait_time = blink_time
		timer.start()


func _process_moving_first_arrow():
	if timer.time_left <= 0:
		timer.wait_time = move_time
		timer.start()


func _process_input():
	var horizontal_move = \
			int(Input.is_action_just_pressed("ui_right")) \
			- int(Input.is_action_just_pressed("ui_left"))
	var vertical_move = \
			int(Input.is_action_just_pressed("ui_down")) \
			- int(Input.is_action_just_pressed("ui_up"))
	var move = Vector2(horizontal_move, vertical_move)

	_move(move, small_origin.global_position)


func _process_wait_moving_arrow():
	if tween.is_active(): return
	state = MOVING_PLAYER
	if not (level == null or arrows.size() == 0):
		level.move_player(arrows[0].direction)
	_on_timeout()


func _process_moving_player():
	if not level == null and level.player.tween.is_active(): return
	if arrows.size() == level.faces:
		state = CHOOSING_MOVE
	else:
		state = WAITING_INPUT
	_on_timeout()


func _process_spawning_hint_arrow():
	var position: Vector2 = $Selector.global_position \
			if arrows.size() == 0 else arrows[arrows.size() - 1].ref.global_position
	if not level == null:
		$Hint.global_position = position
		interpolate_global_position($Hint,
			(level.player.global_position + player_direction * level.player.size) \
				- level.player.get_camera_offset(), 0.5,
			Tween.TRANS_SINE, Tween.EASE_IN_OUT
		)
		tween.start()
	$Hint.rotation_degrees = $Selector.rotation_degrees
	$Hint.visible = true
	$Wave.frame = 0
	$Wave.play("wave")
	state = WAIT_HINT_ARROW


func _process_wait_hint_arrow():
	if not tween.is_active():
		$Hint.visible = false
		state = SHIFT_ARROWS if is_boost else WAITING_INPUT
		if not level == null:
			level.move_player(player_direction)
		_on_timeout()


func _process_shift_arrows():
	if tween.is_active(): return
	state = DROP_ARROW if is_boost else WAITING_INPUT
	_on_timeout()


func _blinking_arrow_finished():
	if arrows.size() == 0:
		state = WAITING_INPUT
		return

	arrows[0].ref.stop_blinking()
	state = MOVING_FIRST_ARROW


func _orient_arrows_finished():
	for arrow in arrows:
		arrow.ref.sprite_rotation_degrees = - rad2deg(player_direction.angle_to(Vector2.RIGHT))
		arrow.direction = player_direction
	state = BOOSTING_PLAYER
	timer.wait_time = orient_delay
	timer.start()


func _moving_first_arrow_finished():
	if arrows.size() == 0:
		state = WAITING_INPUT
		return
	var faces = default_faces if level == null else level.faces

	var arrow: Node2D = arrows[0].ref
	var target := Vector2(
			big_origin.global_position.x + spacing_x + size / 2,
			big_origin.global_position.y \
			- (min(faces, arrows.size()) - 1) \
			* (size + spacing_y) - size / 2 - spacing_y
	)

	interpolate_global_position(arrow, target, move_time, Tween.TRANS_SINE)
	tween.start()
	state = WAIT_MOVING_ARROW


func _shift_arrows():
	for face in range(0, arrows.size()):
		var arrow: Node2D = arrows[face].ref
		interpolate_global_position(arrow,
				position_below(arrow.global_position), move_time
		)
	interpolate_global_position($Selector,
			position_below($Selector.global_position), move_time
	)
	tween.start()


func _drop_arrow_finished():
	if arrows.size() == 0:
		state = WAITING_INPUT
		return
	arrows.pop_back().ref.queue_free()
	state = BOOSTING_PLAYER if is_boost else WAITING_INPUT
	_on_timeout()


func _animate_backgrounds():
	$Remote/ScreenBig.region_rect.position -= Vector2(0, bck_scroll)
	$Remote/ScreenSmall.region_rect.position -= Vector2(0, bck_scroll)


func _choose_next_move():
	if random_round_target == null:
		random_round_target = random_rounds * arrows.size() \
				+ rng.randi_range(0, arrows.size() - 1)

	if random_round_current == null:
		random_round_current = 0
	elif random_round_current == random_round_target:
		if not level == null:
			player_direction = \
					arrows[random_round_target % arrows.size() - 1].direction
		state = ORIENT_ARROWS
		timer.wait_time = orient_delay
		timer.start()
		random_round_target = null; random_round_current = null
		return
	else:
		$Selector.visible = true
		$Selector.global_position = \
		arrows[random_round_current % arrows.size()].ref.global_position
		$Selector.rotation_degrees = \
			arrows[random_round_current % arrows.size()].ref.get_arrow_rot()
		random_round_current += 1

	if timer.time_left <= 0:
		timer.wait_time = choose_time / random_round_target
		timer.start()


func _boosting_player():
	$Selector.visible = false
	if arrows.size() == 0:
		is_boost = false
		if not level == null:
			level.enemies_can_move = true
		state = WAITING_INPUT
		return

	if not level == null:
		level.enemies_can_move = true # set to false if you don't want the enemies to move during your boost
	is_boost = true
	state = SPAWNING_HINT_ARROW


func _move(direction, position):
	match direction:
		Vector2.RIGHT: commit(Vector2.RIGHT, position)
		Vector2.UP: commit(Vector2.UP, position)
		Vector2.LEFT: commit(Vector2.LEFT, position)
		Vector2.DOWN: commit(Vector2.DOWN, position)


func _on_timeout():
	match state:
		BLINKING_ARROW: _blinking_arrow_finished()
		ORIENT_ARROWS: _orient_arrows_finished()
		MOVING_FIRST_ARROW: _moving_first_arrow_finished()
		SHIFT_ARROWS: _shift_arrows()
		DROP_ARROW: _drop_arrow_finished()
		CHOOSING_MOVE: _choose_next_move()
		BOOSTING_PLAYER: _boosting_player()
