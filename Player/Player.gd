# SPDX-FileCopyrightText: 2022 Adriel Dumas--Jondeau <adrieldj@orange.fr>
# SPDX-FileCopyrightText: 2022 Luc Deligne <lucdeligne@hotmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

extends Node2D


signal moved()

export(float) var move_time = 0.1
export(int) var size = 16
export(String, "right", "up", "left", "down") var base_direction = "left"

enum { MOVING, TRY_BONK, RETURN_BONK, WAITING_TURN }

var state := WAITING_TURN
var bonk_direction := Vector2.ZERO

onready var tween := $Tween
onready var anim := $AnimatedSprite
onready var camera := $Camera2D


func _ready():
	anim.animation = base_direction


func _process(_delta):
	match state:
		MOVING: _process_moving()
		TRY_BONK: _process_try_bonk()
		RETURN_BONK: _process_return_bonk()


func _process_moving():
	if tween.is_active(): return
	emit_signal("moved")
	anim.play("bonk_" + anim.animation)
	state = WAITING_TURN


func _process_try_bonk():
	if tween.is_active(): return
	state = RETURN_BONK
	anim.play("bonk_" + anim.animation)
	Effect.emit_signal("screen_shake", 0.1, 4.0)
	tween.interpolate_property(self, "position",
		position, position - bonk_direction * size * 0.3, move_time / 2,
		Tween.TRANS_SINE, Tween.EASE_OUT
	)
	tween.start()


func _process_return_bonk():
	if tween.is_active(): return
	emit_signal("moved")
	state = WAITING_TURN


func get_camera_offset() -> Vector2:
	return camera.get_camera_screen_center() - get_viewport_rect().size / 2


func set_camera_limits(lim: Vector2):
	camera.limit_bottom = lim.y
	camera.limit_left = 0
	camera.limit_right = lim.x
	camera.limit_top = 0


func bonk(direction: Vector2):
	state = TRY_BONK
	bonk_direction = direction
	_set_sprite_direction(direction)
	tween.interpolate_property(self, "position",
		position, position + bonk_direction * size * 0.3, move_time / 2,
		Tween.TRANS_SINE, Tween.EASE_IN
	)
	tween.start()


func move(direction: Vector2):
	state = MOVING
	_set_sprite_direction(direction)
	Effect.emit_signal("screen_shake", 0.1, 2.0)
	tween.interpolate_property(self, "position",
		position, position + direction * size, move_time,
		Tween.TRANS_SINE, Tween.EASE_OUT
	)
	tween.start()


func _set_sprite_direction(direction: Vector2):
	match direction:
		Vector2.RIGHT: anim.play("right")
		Vector2.UP: anim.play("up")
		Vector2.LEFT: anim.play("left")
		Vector2.DOWN: anim.play("down")


func _on_AnimatedSprite_animation_finished():
	anim.play(anim.animation.trim_prefix("bonk_"))
