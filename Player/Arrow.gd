# SPDX-FileCopyrightText: 2022 Adriel Dumas--Jondeau <adrieldj@orange.fr>
# SPDX-FileCopyrightText: 2022 Luc Deligne <lucdeligne@hotmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

extends Node2D

var sprite_rotation_degrees setget set_arrow_rot, get_arrow_rot


func set_arrow_rot(rotation_degrees: int):
	$Sprite.rotation_degrees = rotation_degrees


func get_arrow_rot() -> int:
	return $Sprite.rotation_degrees


func start_blinking():
	$Sprite.play("flash")


func stop_blinking():
	$Sprite.stop()
	$Sprite.frame = 0
