# SPDX-FileCopyrightText: 2022 Adriel Dumas--Jondeau <adrieldj@orange.fr>
# SPDX-FileCopyrightText: 2022 Luc Deligne <lucdeligne@hotmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

extends TileMap


const path_name := "path"

var _point_path = []
var _half_cell := cell_size / 2


var path_tile_id := tile_set.find_tile_by_name(path_name)
onready var path_tiles := get_used_cells_by_id(path_tile_id)
onready var astar_node = AStar2D.new()
onready var map_size = get_used_rect().size


func _ready():
	_astar_setup(astar_node)


# ASTAR SETUP


func _astar_setup(astar: AStar2D) -> void:
	astar.clear()
	_astar_add_path_cells(astar, path_tiles)
	_astar_connect_path_cells(astar, path_tiles)


func _astar_add_path_cells(astar: AStar2D, cell_list := []) -> void:
	for cell in cell_list:
		var cell_index := _calculate_point_index(cell)
		astar.add_point(cell_index, cell)


func _astar_connect_path_cells(astar: AStar2D, cell_list: Array):
	for cell in cell_list:
		var cell_index := _calculate_point_index(cell)
		var cell_relatives := PoolVector2Array([
			cell + Vector2.RIGHT,
			cell + Vector2.LEFT,
			cell + Vector2.UP,
			cell + Vector2.DOWN])
		for relative in cell_relatives:
			var relative_index := _calculate_point_index(relative)
			if relative in path_tiles:
				astar.connect_points(cell_index, relative_index, false)



# PATH-FINDING


func pathfind_to_position(start_pos: Vector2, end_pos: Vector2) -> Dictionary:
	var path_to_goal := _astar_find_path(start_pos, end_pos)
	if path_to_goal.size() > 1:
		var cur_pos := map_to_world(world_to_map(start_pos)) + _half_cell
		var next_pos : Vector2 = path_to_goal[1]
		var direction: Vector2 = next_pos - cur_pos
		return {"direction": direction, "distance": path_to_goal.size()}
	else:
		return {"direction": Vector2.ZERO, "distance": 1}


func _astar_find_path(world_start: Vector2, world_end: Vector2) -> PoolVector2Array:
	var path_start := world_to_map(world_start)
	var path_end := world_to_map(world_end)
	assert(path_tiles.has(path_start))
	assert(path_tiles.has(path_end))
	
	var start_idx := _calculate_point_index(path_start)
	var end_idx := _calculate_point_index(path_end)
	var astar_path : PoolVector2Array = astar_node.get_point_path(start_idx, end_idx)
	
	var world_path : PoolVector2Array = []
	for point in astar_path:
		var world_point = map_to_world(point) + _half_cell
		world_path.append(world_point)
	return world_path



# UTILITY


func _calculate_point_index(cell: Vector2) -> int:
	return cell.x + map_size.x * cell.y


func get_car_position(car) -> Vector2:
	if car is Vector2:
		return world_to_map(to_local(car))
	else:
		return world_to_map(to_local(car.global_position))


func is_tile_path(pos: Vector2) -> bool:
	return path_tiles.has(world_to_map(to_local(pos)))
