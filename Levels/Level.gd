# SPDX-FileCopyrightText: 2022 Adriel Dumas--Jondeau <adrieldj@orange.fr>
# SPDX-FileCopyrightText: 2022 Luc Deligne <lucdeligne@hotmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

extends Node2D


signal game_ended(result)


export(int) var faces = 6
export(float) var enemy_move_time = 0.1

enum { PLAYER_MOVING, ENEMIES_MOVING, WAITING_ENEMIES, GAME_ENDED }

var state := PLAYER_MOVING
var enemies_can_move := true
var enemies := []
var claims = null
var finish_line := Rect2(Vector2.ZERO, Vector2.ZERO)

onready var timer: Timer = $Timer
onready var goal: Position2D = get_node_or_null("Goal")
onready var player: Node2D = $Sort/Player
onready var world: TileMap = $TileMap


func _ready():
	assert(not world == null) # keep cool and nobody explodes
	assert(not player == null)

	var finish_line_ref: ReferenceRect = get_node_or_null("FinishLine")
	if not finish_line_ref == null:
		finish_line = Rect2(finish_line_ref.rect_position, finish_line_ref.rect_size)

	var enemies_container: Node = get_node_or_null("Sort/Enemies")
	if not enemies_container == null:
		enemies = enemies_container.get_children()

	player.set_camera_limits(world.get_used_rect().size * world.cell_size.x)


func _process(_delta):
	match state:
		ENEMIES_MOVING: _process_enemies()
		WAITING_ENEMIES: _process_waiting_enemies()


func _process_enemies():
	if claims == null:
		claims = []
		# Since when do you care about type-safety, Godot?
		# warning-ignore:incompatible_ternary
		var goal_position = null if goal == null else goal.global_position
		for enemy in enemies:
			var data = enemy.claim_position(world, goal_position, player)
			data.enemy = enemy
			claims.push_back(data)

		claims.sort_custom(self, "sort_ascending_by_distance")
		return

	if timer.time_left <= 0:
		timer.wait_time = enemy_move_time
		timer.start()

	if claims.size() == 0:
		claims = null
		state = WAITING_ENEMIES


func _process_waiting_enemies():
	for enemy in enemies:
		if enemy.tween.is_active(): continue
		if check_game_ended("lose", enemy): return


func check_game_ended(sig: String, obj: Node2D) -> bool:
	var victory = finish_line.has_point(obj.global_position)
	if victory:
		emit_signal("game_ended", sig)
		state = GAME_ENDED
	return victory


static func sort_ascending_by_distance(a, b):
	return a.distance < b.distance


func move_player(direction: Vector2):
	var player_next_pos: Vector2 = player.global_position + direction * player.size

	if not world.is_tile_path(player_next_pos):
		player.bonk(direction)
		return
	for enemy in enemies:
		if world.get_car_position(player_next_pos) == world.get_car_position(enemy):
			player.bonk(direction)
			return
	player.move(direction)


func _on_Player_moved():
	if check_game_ended("win", player): return
	state = ENEMIES_MOVING if enemies_can_move else PLAYER_MOVING


func _enemy_moving_finished():
	if claims == null or claims.size() == 0: return
	var claim = claims.pop_front()
	var claim_position: Vector2 = world.get_car_position(claim.position)
	if claim_position == world.get_car_position(player.global_position):
		claim.enemy.bonk(claim.position)
		return
	for enemy in enemies:
		if world.get_car_position(enemy.global_position) == claim_position:
			claim.enemy.bonk(claim.position)
			return
	if world.is_tile_path(claim.position):
		claim.enemy.move(claim.position)


func _on_Timer_timeout():
	match state:
		ENEMIES_MOVING: _enemy_moving_finished()
