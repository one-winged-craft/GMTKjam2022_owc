# SPDX-FileCopyrightText: 2022 Adriel Dumas--Jondeau <adrieldj@orange.fr>
# SPDX-FileCopyrightText: 2022 Luc Deligne <lucdeligne@hotmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

extends Node2D


signal screen_faded()
signal screen_cleared()

# pause_time is the time the screen stays dark (before fading out)
func fade(pause_time:= 0.4) -> void:
	$AnimationPlayer.play("Intro")
	$Timer.wait_time = pause_time


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Intro":
		$Timer.start()
	else:
		emit_signal("screen_cleared")


func _on_Timer_timeout():
	emit_signal("screen_faded")
	$AnimationPlayer.play("Outro")
