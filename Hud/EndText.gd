# SPDX-FileCopyrightText: 2022 Adriel Dumas--Jondeau <adrieldj@orange.fr>
# SPDX-FileCopyrightText: 2022 Luc Deligne <lucdeligne@hotmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

extends Node2D


signal animation_over()

export(float) var wait_time := 0.5

onready var timer: Timer = $Timer


func won_text() -> void:
	_intro(true)


func lost_text() -> void:
	_intro(false)


func _intro(won: bool) -> void:
	if won:
		$Top.frame = 0
		$Bottom.frame = 2
	else:
		$Top.frame = 1
		$Bottom.frame = 3
	$AnimationPlayer.play("Intro")


func _on_AnimationPlayer_animation_finished(anim):
	match anim:
		"Intro":
			Effect.emit_signal("screen_shake", 0.2, 4.0)
			timer.wait_time = wait_time
			timer.start()
		"Outro":
			emit_signal("animation_over")


func _on_Timer_timeout():
	$AnimationPlayer.play("Outro")
