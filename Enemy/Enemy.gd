# SPDX-FileCopyrightText: 2022 Adriel Dumas--Jondeau <adrieldj@orange.fr>
# SPDX-FileCopyrightText: 2022 Luc Deligne <lucdeligne@hotmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

extends Node2D


export(float) var how_good = 0.8 # It have 80 % chance of taking the right direction
export(String, "right", "up", "left", "down") var base_direction = "left"
export(float) var move_time = 0.1
export(int) var size = 16

enum { STILL, MOVING, TRY_BONK, RETURN_BONK }

var state := STILL
var bonk_position := Vector2.ZERO
var rng := RandomNumberGenerator.new()

onready var tween := $Tween
onready var anim := $AnimatedSprite


func _ready():
	$AnimatedSprite.animation = base_direction
	rng.randomize()


func _process(_delta):
	match state:
		MOVING: _process_moving()
		TRY_BONK: _process_try_bonk()
		RETURN_BONK: _process_return_bonk()


func _process_moving():
	if tween.is_active(): return
	anim.play("bonk_" + anim.animation)
	state = STILL


func _process_try_bonk():
	if tween.is_active(): return
	tween.interpolate_property(self, "global_position",
		global_position, bonk_position, move_time / 2,
		Tween.TRANS_SINE, Tween.EASE_OUT
	)
	tween.start()
	state = RETURN_BONK


func _process_return_bonk():
	_process_moving()


func claim_position(world, target, player):
	var data = null
	var omen := 0.0

	if target == null:
		data = { direction = get_random_direction() * size, distance = INF }
	else:
		data = world.pathfind_to_position(global_position, target)
		if not player == null:
			var player_data = world.pathfind_to_position(player.global_position, target)
			omen = 1 - float(data.distance) / float(player_data.distance)
		if rng.randf() > max(how_good - omen, 0.2):
			data.direction = get_random_direction(data.direction) * size

	_set_sprite_direction(data.direction)
	return { position = global_position + data.direction, distance = data.distance }


func get_random_direction(except = null) -> Vector2:
	var cardinals := [ Vector2.RIGHT, Vector2.UP, Vector2.LEFT, Vector2.DOWN ]
	if except == null: return cardinals[rng.randi_range(0, 3)]
	cardinals.erase(except)
	return cardinals[rng.randi_range(0, cardinals.size() - 1)]


func bonk(target_position):
	state = TRY_BONK
	bonk_position = global_position
	tween.interpolate_property(self, "global_position",
		global_position, target_position - (0.7 * (target_position - global_position)), move_time / 2,
		Tween.TRANS_SINE, Tween.EASE_IN
	)
	tween.start()


func move(target):
	state = MOVING
	tween.interpolate_property(self, "global_position",
		global_position, target, move_time,
		Tween.TRANS_SINE, Tween.EASE_OUT
	)
	tween.start()


func _set_sprite_direction(direction: Vector2):
	match direction.normalized():
		Vector2.RIGHT: anim.animation = "right"
		Vector2.UP: anim.animation = "up"
		Vector2.LEFT: anim.animation = "left"
		Vector2.DOWN: anim.animation = "down"


func _on_AnimatedSprite_animation_finished():
	anim.play(anim.animation.trim_prefix("bonk_"))
