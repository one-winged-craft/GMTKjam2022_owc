# SPDX-FileCopyrightText: 2022 Adriel Dumas--Jondeau <adrieldj@orange.fr>
# SPDX-FileCopyrightText: 2022 Luc Deligne <lucdeligne@hotmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

extends Node


signal screen_shake(duration, strength)


func _ready() -> void:
	randomize()


static func screen_freeze(duration: int) -> void:
	OS.delay_msec(duration)
